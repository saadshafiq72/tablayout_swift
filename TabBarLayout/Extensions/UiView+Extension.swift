//
//  UiView+Extension.swift
//  TabBarLayout
//
//  Created by softech on 11/06/2021.

import UIKit

//@IBDesignable
extension UIView {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}


private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        if let t = textField.text {
            textField.text = String(t.prefix(maxLength))
        }
    }
    
    @IBInspectable var leftSpace:CGFloat {
        get {
            return leftView?.frame.size.width ?? 0
        } set {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: 40))
            view.backgroundColor = .clear
            leftView = view
            leftViewMode = .always
        }
    }
    
    @IBInspectable var rightSpace:CGFloat {
        get {
            return rightView?.frame.size.width ?? 0
        } set {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: 40))
            view.backgroundColor = .clear
            rightView = view
            rightViewMode = .always
        }
    }
    
    @IBInspectable var rightImage:String? {
        get {
            
            return ""
        }
        set {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.height, height: frame.height))
            view.backgroundColor = .clear
            let image = UIImage(named: newValue ?? "")
            let imgView = UIImageView(image: image)
            imgView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            view.addSubview(imgView)
            imgView.center = view.center
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            imgView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            rightView = view
            rightViewMode = .always
        }
    }
    
    @IBInspectable var leftImageColor:UIColor? {
        get {
            return nil
        }
        set {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { [weak self] in
                let img = (self?.leftView?.subviews.first as? UIImageView)?.image?.withRenderingMode(.alwaysTemplate)
                let imgView = (self?.leftView?.subviews.first as? UIImageView) //?.image = img
                imgView?.tintColor = newValue
                imgView?.image = img
            }
        }
    }
    
    @IBInspectable var leftImage:String? {
        get {
            return ""
        }
        set {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: frame.height, height: frame.height))
            view.backgroundColor = .clear
            let imgView = UIImageView(image: UIImage(named: newValue ?? ""))
            imgView.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            view.addSubview(imgView)
            imgView.center = view.center
            view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            imgView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            //view.widthAnchor.constraint(lessThanOrEqualToConstant: 40).isActive = true
            leftView = view
            leftViewMode = .always
        }
    }
    
}
