//
//  ProfileViewController.swift
//  TabBarLayout
//
//  Created by softech on 21/06/2021.
//

import UIKit
import IQKeyboardManagerSwift

class ProfileViewController: UIViewController {

    @IBOutlet weak var btnShowHide: UIButton!
    var isShowPassword = false
    var isShowConfirmPassword = false
    
    @IBOutlet weak var btnHideShow: UIButton!
    @IBOutlet weak var confirmPassfield: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
       // IQKeyboardManager.shared.enable = true
    }
    @IBAction func hideConfirmPassBtn(_ sender: Any) {
        
        if isShowConfirmPassword == false {
            confirmPassfield.isSecureTextEntry = false
            btnHideShow.setImage(UIImage(named: "Unhide_eye"), for: .normal)
            isShowConfirmPassword = true
        } else {
            confirmPassfield.isSecureTextEntry = true
            btnHideShow.setImage(UIImage(named: "Hide_eye"), for: .normal)
            isShowConfirmPassword = false
        }
    }
    
    deinit {
      //  IQKeyboardManager.shared.enable = false
    }
    

    // MARK: - Navigation

    
    @IBAction func hidePasswordBtn(_ sender: Any) {
        
        if isShowPassword == false {
            passwordField.isSecureTextEntry = false
            btnShowHide.setImage(UIImage(named: "Unhide_eye"), for: .normal)
            isShowPassword = true
        } else {
            passwordField.isSecureTextEntry = true
            btnShowHide.setImage(UIImage(named: "Hide_eye"), for: .normal)
            isShowPassword = false
        }
        
        
    }


}
