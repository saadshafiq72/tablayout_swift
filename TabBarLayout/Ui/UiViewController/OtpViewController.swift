//
//  OtpViewController.swift
//  TabBarLayout
//
//  Created by softech on 25/06/2021.
//

import UIKit

class OtpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    // MARK: - Navigation

 
    @IBAction func continueBtn(_ sender: Any) {
        
        let profileSetupVC = self.storyboard?.instantiateViewController(withIdentifier: ProfileViewController.className) as! ProfileViewController
        self.navigationController?.pushViewController(profileSetupVC, animated: true)
    }
    
}
