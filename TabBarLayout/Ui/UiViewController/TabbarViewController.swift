//
//  ViewController.swift
//  TabBarLayout
//
//  Created by softech on 01/06/2021.
//

import UIKit
import Foundation
import CountryPickerView
import IQKeyboardManagerSwift

class TabbarViewController: UIViewController {
    
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var tab2: UIButton!
    @IBOutlet weak var tab1: UIButton!
    @IBOutlet var collectionViewCity : UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    private var halfScreenSize : CGFloat?
    var isShowPassword = false
    
    var countryNameSignUp = "Select Country"
    var countryCodeSignUp = "+00"
    var signUp = false
    var login = false
    var countryNameLogin = "Select Country"
    var countryCodeLogin = "+00"
    
    let signUpCountryPicker = CountryPickerView()
    let loginCountryPicker = CountryPickerView()
    
    override func viewDidLoad() {
        
        
        self.collectionViewCity.register(UINib.init(nibName: "LoginCell", bundle: nil), forCellWithReuseIdentifier: "LoginCell")
        
        self.collectionViewCity.register(UINib.init(nibName: "SignupCell", bundle: nil), forCellWithReuseIdentifier: "SignupCell")
        
        let screenSize = UIScreen.main.bounds
        halfScreenSize = screenSize.width / 2
        
        signUpCountryPicker.delegate = self
        loginCountryPicker.delegate = self
        
        

    }
    
    //MARK:- IBActions
    
    
    
    @IBAction func tab1(_ sender: Any) {
       
        collectionViewCity.scrollToItem(at: IndexPath(item: 0, section: 0),
                                        at: .left, animated: true)
        
        moveIndicatorLeft()
    }
    @IBAction func tab2(_ sender: UIButton) {
        
        self.collectionViewCity.isPagingEnabled = false
        collectionViewCity.scrollToItem(at: IndexPath(item: 1, section: 0),
                                        at: .right, animated: true)
        self.collectionViewCity.isPagingEnabled = true
        
        moveIndicatorRight()
        
    }
    
    func moveIndicatorRight() {
        
        tab2.setTitleColor(.systemBlue, for: .normal)
        tab1.setTitleColor(.black, for: .normal)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.indicator.frame.origin = CGPoint(x:self.halfScreenSize! , y: self.indicator.frame.origin.y)
            
        }, completion: { (value: Bool) in
            
        })
    }
    
    func moveIndicatorLeft() {
        
        tab2.setTitleColor(.black, for: .normal)
        tab1.setTitleColor(.systemBlue, for: .normal)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.indicator.frame.origin = CGPoint(x:0 , y: self.indicator.frame.origin.y)
            
        }, completion: { (value: Bool) in
            
        })
    }
    
    func layoutCollectionView() {
            //let edgeInset:CGFloat =  16
            
            //let width = collectionView.frame.size.width   //UIScreen.main.bounds.size.width
            //let height = collectionView.frame.size.height //width * 200 / 160
        
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .horizontal        //.vertical
            layout.itemSize = collectionViewCity.frame.size //CGSize(width: width, height: height)
            layout.minimumInteritemSpacing = 0          //10.0 //* widthRation // Veritical Space
            layout.minimumLineSpacing = 0               //10.0 //* widthRation      // HorizSpace
            //layout.headerReferenceSize = CGSize(width: 0, height: 35)
            layout.sectionInset = UIEdgeInsets.zero     //(top: edgeInset, left: edgeInset, bottom: edgeInset, right: edgeInset)
            collectionViewCity.collectionViewLayout = layout
        collectionViewCity.reloadData()
        }
    
    override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            layoutCollectionView()
        }
    
}




extension TabbarViewController : UICollectionViewDelegate , UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cellLogin = collectionView.dequeueReusableCell(withReuseIdentifier: "LoginCell", for: indexPath) as!  LoginCell
            cellLogin.btnSelectCountry.setTitle(countryNameLogin, for: .normal)
            cellLogin.btnSelectCountry.tag = indexPath.row
            cellLogin.btnSelectCountry.addTarget(self, action: #selector(self.ShowLoginCountryPicker), for: .touchUpInside)
            cellLogin.phoneCodeLbl.text = countryCodeLogin
            return cellLogin
            
        }
        else {
            let signupCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SignupCell", for: indexPath) as!  SignupCell
            signupCell.selectCountryBtn.setTitle(countryNameSignUp, for: .normal)
            signupCell.selectCountryBtn.tag = indexPath.row
            signupCell.selectCountryBtn.addTarget(self, action: #selector(self.ShowSignupCountryPicker), for: .touchUpInside)
                        signupCell.phoneCodeLbl.text = countryCodeSignUp
            signupCell.SignUpBtn.tag = indexPath.row
            signupCell.SignUpBtn.addTarget(self, action: #selector(self.SignupAction), for: .touchUpInside)
            return signupCell
        }
        
    }
    
    @objc func SignupAction(sender: UIButton) {
        
        let profileSetupVC = self.storyboard?.instantiateViewController(withIdentifier: OtpViewController.className) as! OtpViewController
        self.navigationController?.pushViewController(profileSetupVC, animated: true)
    }
    
    @objc func ShowLoginCountryPicker(sender : UIButton){

        login = true
        signUp = false
        loginCountryPicker.showCountriesList(from: self)
    }
    
    @objc func ShowSignupCountryPicker(sender : UIButton){
        signUp = true
        login = false
        signUpCountryPicker.showCountriesList(from: self)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            
            pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
            
            if(pageControl.currentPage == 0){
                
                moveIndicatorLeft()
            }
            else{
                
                moveIndicatorRight()
            }
    }
}
    
    extension TabbarViewController: CountryPickerViewDelegate {
        func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
                
            if(signUp){
                countryNameSignUp = country.name
                countryCodeSignUp = country.phoneCode
                collectionViewCity.reloadItems(at: [IndexPath(row: 1, section: 0)])
            }
            else{
                countryNameLogin = country.name
                countryCodeLogin = country.phoneCode
                collectionViewCity.reloadItems(at: [IndexPath(row: 0, section: 0)])
            }
            

        }
        
    }


