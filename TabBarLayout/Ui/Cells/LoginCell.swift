//
//  CollectionViewCell.swift
//  TabBarLayout
//
//  Created by softech on 02/06/2021.
//

import UIKit

class LoginCell: UICollectionViewCell {

    @IBOutlet weak var passwordTextFiled: UITextField!
    @IBOutlet weak var btnShowHide: UIButton!
    var isShowPassword = false
    @IBOutlet weak var phoneCodeLbl: UILabel!
    
    @IBOutlet weak var editPhoneNumber: UITextField!
    @IBOutlet weak var btnSelectCountry: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func hidePasswordBtn(_ sender: Any) {
        
        if isShowPassword == false {
            passwordTextFiled.isSecureTextEntry = false
            btnShowHide.setImage(UIImage(named: "Unhide_eye"), for: .normal)
            isShowPassword = true
        } else {
            passwordTextFiled.isSecureTextEntry = true
            btnShowHide.setImage(UIImage(named: "Hide_eye"), for: .normal)
            isShowPassword = false
        }
        
        
    }
}
